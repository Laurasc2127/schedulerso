
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

 void *hilos(void *t)
 {
    int i;
    long hiloID;


    hiloID = (long)t;
    printf("\n\n-| No %ld leyendo un libro \n",hiloID);

    printf("\n\n-> No %ld  escribiendo un mensaje \n",hiloID);

     printf("\n\n-> No %ld  escuchando musica \n",hiloID);
    pthread_exit((void*) t);
 }

 int noHilos;

 void numeroH (){
    printf("\n Ingrese hilos \n");
    scanf ("%d",&noHilos);
 }

 int main (int argc, char *argv[])
 {
    numeroH();
    pthread_t thread[noHilos];
    pthread_attr_t at;
    int r;
    long t;
    void *st;


    pthread_attr_init(&at);
    pthread_attr_setdetachstate(&at, PTHREAD_CREATE_JOINABLE);

    for(t=0; t<noHilos  ; t++) {
       printf("\n\n hilo creado N %ld\n", t);
       r = pthread_create(&thread[t], &at, hilos, (void *)t);
       if (r) {
          printf("\n> >ERROR;  %d\n", r);
          exit(-1);
          }
       }

    pthread_attr_destroy(&at);
    for(t=0; t<noHilos; t++) {
       r = pthread_join(thread[t], &st);
       if (r) {
          }
       printf("\n proceso de hilo %ld completado....\n",t);
       }

 }
